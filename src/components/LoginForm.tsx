// libraries
import React, { useState, useEffect } from "react";
import { bindActionCreators } from "redux";
import { makeStyles, createStyles, Theme } from "@material-ui/core";

// colors
import { green } from "@material-ui/core/colors";

// hoc
import { connect } from "react-redux";

// components
import { ThunkDispatch } from "redux-thunk";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import {
  FormControl,
  FormGroup,
  FormControlLabel,
  Checkbox,
  InputAdornment,
  IconButton,
  Button,
  CircularProgress
} from "@material-ui/core";

// icons
import { Visibility, VisibilityOff } from "@material-ui/icons";

// actions
import { userActions } from "../actions";

// types
import { AppState } from "../reducers";
import { AuthState, AppActions } from "../types";

// styles
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      alignItems: "center"
    },
    wrapper: {
      margin: theme.spacing(1),
      position: "relative"
    },
    buttonProgress: {
      color: green[500],
      position: "absolute",
      top: "50%",
      left: "50%",
      marginTop: -12,
      marginLeft: -12
    }
  })
);

type LoginFormState = {
  email: string;
  password: string;
  remember: boolean;
};

type Props = LinkStateToProps & LinkDispatchToProps;

// login form fc
const LoginForm: React.FC<Props> = (props: Props) => {
  const classes = useStyles();

  const [values, setValues] = useState<LoginFormState>({
    email: "",
    password: "",
    remember: false
  });

  // handle fields change event
  const handleChange = (prop: keyof LoginFormState) => (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    prop === "remember"
      ? setValues({ ...values, [prop]: event.target.checked })
      : setValues({ ...values, [prop]: event.target.value });
  };

  // handle password field
  const [showPassword, setShowPassword] = useState(false);
  const handlePasswordToggle = () => {
    setShowPassword(!showPassword);
  };
  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
  };

  // handle form submit
  const handleSubmit = (event: any): void => {
    event.preventDefault();
    props.login(values);
  };

  useEffect(() => {
    // onComponentDidMount
    ValidatorForm.addValidationRule("minLength", value => {
      if (value.length < 5) {
        return false;
      }
      return true;
    });

    return () => {
      // onComponentUnmount
      ValidatorForm.removeValidationRule("minLength");
    };
  }, []);

  return (
    <ValidatorForm onSubmit={handleSubmit}>
      <FormGroup>
        {/* email input field */}
        <FormControl margin="dense">
          <TextValidator
            label="Email address"
            onChange={handleChange("email")}
            name="email"
            value={values.email}
            validators={["required", "isEmail"]}
            errorMessages={["this field is required.", "email is not valid."]}
          />
        </FormControl>

        {/* password input field */}
        <FormControl margin="dense">
          <TextValidator
            label="Password"
            id="adornment-password"
            type={showPassword ? "text" : "password"}
            value={values.password}
            onChange={handleChange("password")}
            name="password"
            validators={["required", "minLength:5"]}
            errorMessages={[
              "this field is required.",
              "password should be at least 5 characters."
            ]}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handlePasswordToggle}
                    onMouseDown={handleMouseDownPassword}
                  >
                    {showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
        </FormControl>

        {/* remember me input */}
        <FormControlLabel
          control={
            <Checkbox
              checked={values.remember}
              onChange={handleChange("remember")}
              value="remember"
            />
          }
          label="Remember me?"
        />

        {/* submit form button */}
        <FormControl>
          <Button
            color="secondary"
            variant="contained"
            type="submit"
            disabled={props.auth.loggingIn}
          >
            Login
          </Button>
          {props.auth.loggingIn && (
            <CircularProgress size={24} className={classes.buttonProgress} />
          )}
        </FormControl>
      </FormGroup>
    </ValidatorForm>
  );
};

interface LinkStateToProps {
  auth: AuthState;
}

interface LinkDispatchToProps {
  login: ({
    email,
    password,
    remember
  }: {
    email: string;
    password: string;
    remember?: boolean;
  }) => void;
}

const mapStateToProps = (state: AppState): LinkStateToProps => ({
  auth: state.authentication
});

const mapDispatchToProps = (
  dispatch: ThunkDispatch<any, any, AppActions>
): LinkDispatchToProps => ({
  login: bindActionCreators(userActions.login, dispatch)
});

const connectedLoginForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginForm);
export { connectedLoginForm as LoginForm };
