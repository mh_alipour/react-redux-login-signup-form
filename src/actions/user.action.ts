// constants
import {
  USER_LOGIN_FAILURE,
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGOUT,
  USER_REGISTER_FAILURE,
  USER_REGISTER_REQUEST,
  USER_REGISTER_SUCCESS
} from "../constants";

// services
import { userService } from "../services";

// helpers
import { history } from "../helpers";

// types
import { Dispatch } from "redux";
import { AppActions, User } from "../types";
import { AppState } from "../reducers";

// user login action
const login = ({
  email,
  password,
  remember = false
}: {
  email: string;
  password: string;
  remember?: boolean;
}) => {
  return (dispatch: Dispatch<AppActions>, useState: () => AppState) => {
    // logging in
    dispatch(request());

    // send login request to server
    userService.login({ email, password, remember }).then(
      user => {
        // login was successful
        dispatch(success(user));
        history.push("/dashboard");
      },
      error => {
        // login failed
        alert(error);
        dispatch(failure(error));
      }
    );

    function request(): AppActions {
      return { type: USER_LOGIN_REQUEST };
    }
    function success(user: User): AppActions {
      return { type: USER_LOGIN_SUCCESS, payload: user };
    }
    function failure(error: any): AppActions {
      return { type: USER_LOGIN_FAILURE, payload: error };
    }
  };
};

// user register action
const register = ({
  fullName,
  email,
  password
}: {
  fullName: string;
  email: string;
  password: string;
}) => {
  return (dispatch: Dispatch<AppActions>, useState: () => AppState) => {
    // logging in
    dispatch(request());

    // send login request to server
    userService.register({ fullName, email, password }).then(
      () => {
        // register was successful
        dispatch(success());
        window.location.reload(true);
      },
      error => {
        // register failed
        alert(error);
        dispatch(failure(error));
      }
    );

    function request(): AppActions {
      return { type: USER_REGISTER_REQUEST };
    }
    function success(): AppActions {
      return { type: USER_REGISTER_SUCCESS };
    }
    function failure(error: any): AppActions {
      return { type: USER_REGISTER_FAILURE, payload: error };
    }
  };
};

// user logout action
const logout = () => {
  userService.logout();
  history.push("/");
  return { type: USER_LOGOUT };
};

export const userActions: any = {
  login,
  register,
  logout
};
