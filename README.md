# react-redux-login-signup-form
example of how you can use material ui, react, redux, typescript, nestjs all together to create a cool looking login-signup app. hope it serves as a good example to follow ;)

##### this project uses the power of material ui to create an app that has an awesome form validation and toggle light/dark button and covers login, signup and logout actions

also thanks to my beautiful friend [Pejman Hadavi](https://github.com/pejmanhadavi/) for the awesome [real-world-example-nestjs-mongoose-jwt-auth](https://github.com/pejmanhadavi/real-world-example-nestjs-mongoose-jwt-auth) project using nestjs

## screenshots

### light mode
![screen1](https://user-images.githubusercontent.com/48043565/63651286-5817ed00-c768-11e9-8ec3-bec3f80bacc0.jpg)

### dark mode
![screen2](https://user-images.githubusercontent.com/48043565/63651288-59e1b080-c768-11e9-889c-9ba3635f20e5.jpg)

## How-To-Use
> this app requires [real-world-example-nestjs-mongoose-jwt-auth](https://github.com/localXx/real-world-example-nestjs-mongoose-jwt-auth) as backend
1. clone project with command `git clone https://github.com/localXx/react-redux-login-signup-form`
2. change directory to `cd react-redux-login-signup-form`
3. install all dependencies `yarn install`
4. run api mentioned above
5. start the app `yarn start`
6. have fun remember to star ;)

## Author
mohamamd alipour aka ***localX***
